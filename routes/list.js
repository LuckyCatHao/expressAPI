/*
 * @Date: 2022-08-19 10:02:37
 * @LastEditors: Mr.qin
 * @LastEditTime: 2022-08-23 15:05:05
 * @Description:
 */
var express = require('express');
var router = express.Router();

const { responce, responceJson, handleOperate } = require('../utils/responce');

/* GET users listing. */
router.get('/', function (req, res, next) {
	res.send('welcome to list');
});

function getList(limit) {
	const result = [];
	for (let i = 1; i <= limit; i++) {
		result.push({
			id: i,
			date: '2016-05-01',
			name: i + '号纪念馆',
			status: 1,
			address: 'xxx',
		});
	}
	return result;
}

router.post('/venueList', (req, res, next) => {
	// console.log(req);

	res.json({
		code: 200,
		msg: 'success',
		result: {
			code: 200,
			data: getList(req.body.limit || 10),
			message: '数据获取成功',
			successful: true,
			total: 100,
		},
		count: 100,
	});
});

module.exports = router;
